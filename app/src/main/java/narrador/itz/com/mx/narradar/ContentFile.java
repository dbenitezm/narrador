package narrador.itz.com.mx.narradar;

import android.content.Context;
import android.content.res.Resources;
import android.speech.tts.TextToSpeech;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

import narrador.itz.com.mx.narradar.utils.OperationsFiles;

public class ContentFile extends AppCompatActivity implements TextToSpeech.OnInitListener{

    private OperationsFiles operationsFiles;
    private ArrayList<String> contentFile;
    private TextView mTesxtViewContentFile;

    private TextToSpeech textToSpeech;

    private int sizeText;
    private int aux=0;
    private int recu=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_content_file);
        this.recu=getIntent().getIntExtra("name",0);
        mTesxtViewContentFile=findViewById(R.id.text_view_content_file);
        operationsFiles=new OperationsFiles();

        contentFile=readerFile("");
        int contentF=contentFile.size()-1;
        sizeText=contentF;
        String text="";
        for(int i=0;i<=contentF;i++){
            text+=contentFile.get(i);
            text+="\n";
        }
        mTesxtViewContentFile.setText(text);
        textToSpeech = new TextToSpeech( ContentFile.this, this );
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(sizeText>aux) {
                    speak(contentFile.get(aux));
                    aux++;
                }else{
                    speak("se termino el texto");
                }

            }
        });


    }


    public ArrayList<String> readerFile(String url) {
        File archivo = null;
        FileReader fr = null;
        BufferedReader br = null;
        ArrayList<String> texto=new ArrayList<>();
        try {
            InputStream fraw= getResources().openRawResource(recu);
            BufferedReader brin=new BufferedReader(new InputStreamReader(fraw));
            String linea;
            while((linea=brin.readLine())!=null)
            texto.add(linea);

            fraw.close();

        }catch(IOException e){
            Log.e("","");
        }
        return texto;
    }

    @Override
    public void onInit(int status) {
        if ( status == TextToSpeech.LANG_MISSING_DATA | status == TextToSpeech.LANG_NOT_SUPPORTED )
        {
            Toast.makeText( this, "ERROR LANG_MISSING_DATA | LANG_NOT_SUPPORTED", Toast.LENGTH_SHORT ).show();
        }
    }

    private void speak( String str )
    {
        textToSpeech.speak( str, TextToSpeech.QUEUE_FLUSH, null );
        textToSpeech.setSpeechRate( 0.0f );
        textToSpeech.setPitch( 0.0f );
    }

    @Override
    public void onPointerCaptureChanged(boolean hasCapture) {

    }
}
