package narrador.itz.com.mx.narradar;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import narrador.itz.com.mx.narradar.adapters.BookAdapter;
import narrador.itz.com.mx.narradar.models.Book;

public class MainActivity extends AppCompatActivity {

    private ListView listBook;
    private BookAdapter adapterListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        listBook=findViewById(R.id.list_view_book);
        final List<Book> list = new ArrayList<>();
        Book modelo = new Book("RECYCLER VIEW", "DIEGO",R.raw.libro1);
        Book modelo1 = new Book("RECYCLER VIEW", "DIEGO",R.raw.libro2);
        Book modelo2 = new Book("RECYCLER VIEW", "DIEGO",R.raw.libro3);
        Book modelo3 = new Book("RECYCLER VIEW", "DIEGO",R.raw.libro4);
        list.add(modelo);
        list.add(modelo1);
        list.add(modelo2);
        list.add(modelo3);


        adapterListView=new BookAdapter(this,list);
        listBook.setAdapter(adapterListView);
        listBook.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Book modelo=adapterListView.getModelo(position);
                Toast.makeText(MainActivity.this,modelo.getName(),Toast.LENGTH_LONG).show();
                startActivity(new Intent(MainActivity.this,ContentFile.class).putExtra("name",modelo.getRecurso()));
            }
        });
    }
}
