package narrador.itz.com.mx.narradar.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import narrador.itz.com.mx.narradar.R;
import narrador.itz.com.mx.narradar.models.Book;

public class BookAdapter extends BaseAdapter{


    private Context context;
    private List<Book> list=new ArrayList<>();

    public BookAdapter(Context context, List<Book> list) {
        this.context = context;
        this.list = list;
    }


    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {

        return list.get(position);
    }

    public Book getModelo(int position){
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;

        Book modeloBook=list.get(position);
        if (convertView==null){
            LayoutInflater inflater= (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            view=inflater.inflate(R.layout.item_book,parent,false);

        }
        ImageView ivFoto=view.findViewById(R.id.image_view_book);
        TextView tvTitle=view.findViewById(R.id.text_view_title);
        TextView tvDescription=view.findViewById(R.id.text_view_description);
        tvTitle.setText(modeloBook.getName());
        tvDescription.setText(modeloBook.getDescription());
        return view;
    }

}
