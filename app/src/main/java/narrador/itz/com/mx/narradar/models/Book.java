package narrador.itz.com.mx.narradar.models;

public class Book {

    private String name;
    private String description;
    private int recurso;

    public Book(String name, String description, int recurso ){
        this.name = name;
        this.description = description;
        this.recurso=recurso;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getRecurso() {
        return recurso;
    }

    public void setRecurso(int recurso) {
        this.recurso = recurso;
    }
}
